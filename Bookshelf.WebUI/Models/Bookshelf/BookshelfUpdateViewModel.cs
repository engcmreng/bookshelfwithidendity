﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Bookshelf.WebUI.Models.Bookshelf
{
    public class BookshelfUpdateViewModel
    {
        public Entity.Entities.Bookshelf Bookshelf { get; set; }
        public List<SelectListItem> AccessRoles { get; set; }
    }
}
