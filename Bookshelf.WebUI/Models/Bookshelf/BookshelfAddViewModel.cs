﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Bookshelf.WebUI.Models.Bookshelf
{
    public class BookshelfAddViewModel
    {
        public Entity.Entities.Bookshelf Bookshelf { get; set; }
        public List<SelectListItem> AccessRoles { get; set; }
    }
}
