﻿using System.Collections.Generic;

namespace Bookshelf.WebUI.Models.Bookshelf
{
    public class BookshelfIndexViewModel
    {
        public List<BookshelfBook> bookshelfBooks { get; set; }
    }
}
