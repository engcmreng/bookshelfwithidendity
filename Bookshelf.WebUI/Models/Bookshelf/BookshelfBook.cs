﻿using System.Collections.Generic;

namespace Bookshelf.WebUI.Models.Bookshelf
{
    public class BookshelfBook
    {
        public Entity.Entities.Bookshelf Bookshelf { get; set; }
        public List<Entity.Entities.Book> Book { get; set; }
    }
}
