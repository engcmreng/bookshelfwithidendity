﻿using Bookshelf.ACore.Interface;
using Bookshelf.ACore.Concrete;
using Bookshelf.Business.Interface;
using Bookshelf.WebUI.Models.BookPage;
using Bookshelf.WebUI.Services.Abstract;
using Microsoft.AspNetCore.Mvc;
using Bookshelf.Entity.Entities;

namespace Bookshelf.WebUI.Controllers
{
    public class BookPageController : Controller
    {
        IBookService _bookService;
        IBookPageService _bookPageService;
        IBookshelfService _bookshelfService;
        IUserSessionService _userSessionService;

        public BookPageController(IBookshelfService bookshelfService, IBookService bookService, IUserSessionService userSessionService = null, IBookPageService bookPageService = null)
        {
            _bookshelfService = bookshelfService;
            _bookService = bookService;
            _userSessionService = userSessionService;
            _bookPageService = bookPageService;
        }

        [HttpGet]
        public IActionResult Add(int ID)
        {
            IReturnException<object> returnException = new ReturnException<object>();

            Book book = _bookService.Get(ID);

            if (book != null)
            {
                Entity.Entities.Bookshelf bookshelfControl = _bookshelfService.Get(book.BookshelfID, _userSessionService.UserGetSession().UserId);

                if (bookshelfControl != null)
                {
                    returnException = _bookPageService.Add(new BookPage
                    {
                        BookID = book.BookID,
                        BookPageTitle = "Başlık",
                        BookPageText = "İçerik",
                    });

                    return Redirect("/Book/Index/" + book.BookID + "/?status=" + returnException.Status + "&message=" + returnException.Message);

                }
            }
            return Redirect("/Bookshelf/Index?status=false&message=Kütüphaneye ulaşılamadı lütfen daha sonra tekrar deneyin !");

        }

        [HttpGet]
        public IActionResult Delete(int ID)
        {
            IReturnException<object> returnException = new ReturnException<object>();

            BookPage bookPage = _bookPageService.Get(ID);

            if (bookPage != null)
            {
                Book book = _bookService.Get(bookPage.BookID);

                if (book != null)
                {
                    Entity.Entities.Bookshelf bookshelf = _bookshelfService.Get(book.BookshelfID, _userSessionService.UserGetSession().UserId);

                    if (bookshelf != null)
                    {
                        returnException = _bookPageService.Delete(bookPage);

                        return Redirect("/Book/Index/" + book.BookID + "/?status=" + returnException.Status + "&message=" + returnException.Message);
                    }
                }
            }
            return Redirect("/Bookshelf/Index?status=false&message=Kütüphaneye ulaşılamadı lütfen daha sonra tekrar deneyin !");
        }

        [HttpGet]
        public IActionResult Write(int ID)
        {
            BookPage bookPage = _bookPageService.Get(ID);
            if (bookPage != null)
            {

                Book book = _bookService.Get(bookPage.BookID);

                if (book != null)
                {
                    Entity.Entities.Bookshelf bookshelf = _bookshelfService.Get(book.BookshelfID, _userSessionService.UserGetSession().UserId);

                    if (bookshelf != null)
                    {
                        var model = new BookPageModel
                        {
                            BookPage = bookPage
                        };

                        ViewBag.title = book.BookName + "/ " + bookPage.BookPageTitle;
                        return View(model);
                    }
                }
            }
            return Redirect("/Bookshelf/Index?status=false&message=Kütüphaneye ulaşılamadı lütfen daha sonra tekrar deneyin !");
        }

        [HttpPost]
        public IActionResult Write(BookPage bookPage)
        {
            IReturnException<object> returnException = new ReturnException<object>();

            if (ModelState.IsValid)
            {
                BookPage bookPageControl = _bookPageService.Get(bookPage.BookPageID);

                if (bookPageControl != null)
                {
                    Book BookControl = _bookService.Get(bookPageControl.BookID);
                    if (BookControl != null)
                    {
                        Entity.Entities.Bookshelf bookshelf = _bookshelfService.Get(BookControl.BookshelfID, _userSessionService.UserGetSession().UserId);

                        returnException = _bookPageService.Update(bookPage);

                        return Redirect("/BookPage/Write/" + bookPageControl.BookPageID + "/?status=" + returnException.Status + "&message=" + returnException.Message);

                    }
                }
                return Redirect("/Bookshelf/Index?status=false&message=Kütüphaneye ulaşılamadı lütfen daha sonra tekrar deneyin !");
            }
            else
            {
                return View();
            }
        }

    }
}
