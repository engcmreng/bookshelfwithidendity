using Bookshelf.Business.Interface;
using Bookshelf.Business.Concrete;
using Bookshelf.DataAccess.Interface;
using Bookshelf.DataAccess.Repositories;
using Bookshelf.WebUI.Services.Abstract;
using Bookshelf.WebUI.Services.Concrete;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Bookshelf.Entity.Entities;
using Microsoft.EntityFrameworkCore;

namespace Bookshelf.WebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IUserSessionService, UserSessionService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IBookshelfRepository, BookshelfRepository>();
            services.AddScoped<IBookshelfService, BookshelfService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAccessRoleService, AccessRoleService>();
            services.AddScoped<IAccessRoleRepository, AccessRoleRepository>();
            services.AddScoped<IBookRepository, BookRepository>();
            services.AddScoped<IBookService, BookService>();
            services.AddScoped<IBookPageService, BookPageService>();
            services.AddScoped<IBookPageDal, BookPageDal>();

            //services.AddDbContext<BookshelfContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            services.AddSession();
            services.AddDistributedMemoryCache();

            services.AddMvc(option => option.EnableEndpointRouting = false);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseSession();


            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{ID?}");
            });
            //app.UseMvcWithDefaultRoute();
        }
    }
}
