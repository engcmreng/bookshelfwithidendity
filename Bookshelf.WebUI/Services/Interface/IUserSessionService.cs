﻿using Bookshelf.Entity.Entities;

namespace Bookshelf.WebUI.Services.Abstract
{
    public interface IUserSessionService
    {
        void UserSetSession(User user, string key = "User");
        void UserRemoveSession(string key = "User");
        User UserGetSession(string key = "User");
    }
}
