﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;

namespace Bookshelf.Entity.Entities
{
    public class BookshelfContext : IdentityDbContext<User>
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(GetConnectionString());
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //builder.Entity<User>()
            //    .HasMany(e => e.Bookshelf)
            //    .WithOne()
            //    .HasForeignKey(e => e.UserId)
            //    .IsRequired()
            //    .OnDelete(DeleteBehavior.Cascade);

        }

        private static string GetConnectionString()
        {
            const string databaseName = "BookShelf-2";
            const string databasePass = "Qwertyasdfg123!";

            return $"Server=(localdb)\\MSSQLLocalDB;" +
                   $"database={databaseName};" +
                   $"User Id = sa;" +
                   $"Trusted_Connection = True;" +
                   $"MultipleActiveResultSets = True;" +
                   $"pwd={databasePass};" +
                   $"pooling=true;";
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Bookshelf> Bookshelf { get; set; }
        public DbSet<AccessRole> AccessRoles { get; set; }
        public DbSet<BookPage> BookPages { get; set; }
        public DbSet<Book> Books { get; set; }

    }
}
