﻿using System;

namespace Bookshelf.ACore.Interface
{
    public interface IReturnException<T>
    {
        public bool Status { get; set; }
        public Exception Exception { get; set; }
        public String Message { get; set; }
        public T Data { get; set; }
        void SetReturnException(bool Status, String Message, T Data, Exception Exception = null);
    }
}
