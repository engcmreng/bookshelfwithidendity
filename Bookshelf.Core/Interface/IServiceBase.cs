﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Bookshelf.ACore.Interface
{
    public interface IServiceBase<TEntity> where TEntity : class, IEntity, new()
    {
        IReturnException<object> Add(TEntity entity);
        IReturnException<object> Delete(TEntity entity);
        IReturnException<object> Update(TEntity entity);
        List<TEntity> GetAll(Expression<Func<TEntity, bool>> filter = null);
        TEntity Get(Expression<Func<TEntity, bool>> filter = null);
    }
}
