﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Bookshelf.ACore.Interface
{
    public interface IRepositoryBase<Entity>
    {
        int Add(Entity entity);
        int Update(Entity entity);
        int Delete(Entity entity);
        List<Entity> GetList(Expression<Func<Entity, bool>> filter = null);
        Entity Get(Expression<Func<Entity, bool>> filter = null);
    }
}
