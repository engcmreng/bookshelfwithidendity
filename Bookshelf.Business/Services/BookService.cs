﻿using Bookshelf.ACore.Concrete;
using Bookshelf.Business.Interface;
using Bookshelf.DataAccess.Interface;
using Bookshelf.Entity.Entities;
using System.Collections.Generic;

namespace Bookshelf.Business.Concrete
{
    public class BookService : ServiceBase<IBookRepository, Book>, IBookService
    {
        public BookService(IBookRepository bookDal) : base(bookDal)
        {

        }

        public Book Get(int BookID, int BookshelfID)
        {
            return base.Get(b => b.BookID == BookID && b.BookshelfID == BookshelfID);
        }

        public Book Get(int BookID)
        {
            return base.Get(b => b.BookID == BookID);
        }

        public List<Book> GetAll(int BookshelfID)
        {
            return base.GetAll(u => u.BookshelfID == BookshelfID);
        }
    }
}
