﻿using Bookshelf.ACore.Concrete;
using Bookshelf.Business.Interface;
using Bookshelf.DataAccess.Interface;
using Bookshelf.Entity.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace Bookshelf.Business.Concrete
{
    public class AccessRoleService : ServiceBase<IAccessRoleRepository, AccessRole>, IAccessRoleService
    {
        public AccessRoleService(IAccessRoleRepository accessRoleDal) : base(accessRoleDal)
        {
        }

        public List<AccessRole> GetAll()
        {
            return base.GetAll();
        }

        public List<SelectListItem> GetSelectListAll()
        {
            return base.GetAll().Select(a => new SelectListItem { Value = a.AccessRoleID.ToString(), Text = a.AccessRoleExplain.ToString() }).ToList();
        }
    }
}
