﻿using Bookshelf.ACore.Concrete;
using Bookshelf.Business.Interface;
using Bookshelf.DataAccess.Interface;
using Bookshelf.Entity.Entities;
using System.Collections.Generic;

namespace Bookshelf.Business.Concrete
{
    public class BookPageService : ServiceBase<IBookPageDal, BookPage>, IBookPageService
    {
        public BookPageService(IBookPageDal bookPageDal) : base(bookPageDal)
        {

        }

        public BookPage Get(int BookPageID)
        {
            return base.Get(p => p.BookPageID == BookPageID);
        }

        public List<BookPage> GetAll(int BookID)
        {
            return base.GetAll(b => b.BookID == BookID);
        }
    }
}
