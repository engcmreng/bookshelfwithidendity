﻿using Bookshelf.ACore.Concrete;
using Bookshelf.Business.Interface;
using Bookshelf.DataAccess.Interface;
using System.Collections.Generic;

namespace Bookshelf.Business.Concrete
{
    public class BookshelfService : ServiceBase<IBookshelfRepository, Entity.Entities.Bookshelf>, IBookshelfService
    {
        public BookshelfService(IBookshelfRepository bookshelfDal) : base(bookshelfDal)
        {
        }

        public Entity.Entities.Bookshelf Get(int ID, int UserID)
        {
            return base.Get(b => b.BookshelfID == ID && b.User.UserId == UserID);
        }

        public List<Entity.Entities.Bookshelf> GetAll(int UserID)
        {
            return base.GetAll(b => b.User.UserId == UserID);
        }
    }
}
