﻿using Bookshelf.ACore.Interface;
using Bookshelf.Entity.Entities;
using System.Collections.Generic;

namespace Bookshelf.Business.Interface
{
    public interface IBookPageService
    {
        IReturnException<object> Add(BookPage bookPage);
        List<BookPage> GetAll(int BookID);
        BookPage Get(int BookPageID);
        IReturnException<object> Delete(BookPage bookPage);
        IReturnException<object> Update(BookPage bookPage);
    }
}
