﻿using Bookshelf.ACore.Interface;
using Bookshelf.Entity.Entities;

namespace Bookshelf.Business.Interface
{
    public interface IUserService
    {
        IReturnException<object> Add(User user);
        IReturnException<object> Update(User user);
        User getNickname(string UserNickname);
        IReturnException<object> UserLogin(string UserNickname, string UserPassword);
        User Get(int UserID);
    }
}
