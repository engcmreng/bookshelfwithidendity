﻿using Bookshelf.Entity.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Bookshelf.Business.Interface
{
    public interface IAccessRoleService
    {
        List<AccessRole> GetAll();

        List<SelectListItem> GetSelectListAll();
    }
}
