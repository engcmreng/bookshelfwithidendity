﻿using Bookshelf.ACore.Interface;
using System.Collections.Generic;

namespace Bookshelf.Business.Interface
{
    public interface IBookService
    {
        List<Entity.Entities.Book> GetAll(int BookshelfID);
        Entity.Entities.Book Get(int BookID, int BookshelfID);
        Entity.Entities.Book Get(int BookID);
        IReturnException<object> Add(Entity.Entities.Book Book);
        IReturnException<object> Delete(Entity.Entities.Book Book);

    }
}
