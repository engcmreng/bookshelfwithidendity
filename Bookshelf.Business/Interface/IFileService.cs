﻿using Microsoft.AspNetCore.Http;
using Bookshelf.ACore.Interface;

namespace Bookshelf.Business.Interface
{
    public interface IFileService
    {
        IReturnException<object> FileUpload(IFormFile file, string filePath, bool directory = false, string fileName = null);
        IReturnException<object> FileRemove(string filePath, bool directory = false);

    }
}
