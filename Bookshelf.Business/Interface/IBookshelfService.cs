﻿using Bookshelf.ACore.Interface;
using System.Collections.Generic;

namespace Bookshelf.Business.Interface
{
    public interface IBookshelfService
    {
        IReturnException<object> Add(Entity.Entities.Bookshelf bookshelf);
        IReturnException<object> Update(Entity.Entities.Bookshelf bookshelf);
        List<Entity.Entities.Bookshelf> GetAll(int UserID);
        Entity.Entities.Bookshelf Get(int ID, int UserID);
    }
}
