﻿using Bookshelf.ACore.Interface;
using Bookshelf.Entity.Entities;

namespace Bookshelf.DataAccess.Interface
{
    public interface IBookPageDal : IRepositoryBase<BookPage>
    {
    }
}
