﻿using Bookshelf.ACore.Interface;
using Bookshelf.Entity.Entities;


namespace Bookshelf.DataAccess.Interface
{
    public interface IUserRepository : IRepositoryBase<User>
    {
    }
}
