﻿using Bookshelf.ACore.Interface;

namespace Bookshelf.DataAccess.Interface
{
    public interface IBookshelfRepository : IRepositoryBase<Entity.Entities.Bookshelf>
    {
    }
}
