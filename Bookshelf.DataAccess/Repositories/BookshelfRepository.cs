﻿using Bookshelf.ACore.Concrete;
using Bookshelf.DataAccess.Interface;
using Bookshelf.Entity.Entities;

namespace Bookshelf.DataAccess.Repositories
{
    public class BookshelfRepository : RepositoryBase<BookshelfContext, Entity.Entities.Bookshelf>, IBookshelfRepository
    {

    }
}
